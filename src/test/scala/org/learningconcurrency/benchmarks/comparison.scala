package org.learningconcurrency.benchmarks



import scala.concurrent.stm._
import org.scalameter.api._



object CtrieBenchmark
extends PerformanceTest.OfflineReport {
  val sizes = Gen.range("size")(50000, 650000, 100000)
  val datasets = for (size <- sizes) yield (0 until size).map(_.toString).toArray
  val simpleCtries = for (dataset <- datasets) yield {
    val ctrie = new simple.ConcurrentTrie[String, String]
    for (d <- dataset) ctrie.insert(d, "")
    (dataset, ctrie)
  }
  val ctries = for (dataset <- datasets) yield {
    val ctrie = new ConcurrentTrie[String, String]
    for (d <- dataset) ctrie.update(d, "")
    (dataset, ctrie)
  }

  measure method "lookup" config(
    exec.minWarmupRuns -> 20,
    exec.maxWarmupRuns -> 40,
    exec.benchRuns -> 30,
    exec.independentSamples -> 2,
    exec.reinstantiation.frequency -> 2
  ) in {
    using(simpleCtries) curve("no-snapshot-support") in { case (dataset, ctrie) =>
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.lookup(d)
        i += 1
      }
    }

    using(ctries) curve("snapshot-support") in { case (dataset, ctrie) =>
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.apply(d)
        i += 1
      }
    }

    using(ctries) curve("after-snapshot") in { case (dataset, ctrie) =>
      ctrie.snapshot()
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.apply(d)
        i += 1
      }
    }
  }

  measure method "insert" config(
    exec.minWarmupRuns -> 20,
    exec.maxWarmupRuns -> 40,
    exec.benchRuns -> 30,
    exec.independentSamples -> 2,
    exec.reinstantiation.frequency -> 2
  ) in {
    using(simpleCtries) curve("no-snapshot-support") tearDown { case (dataset, ctrie) =>
      //for (d <- dataset) ctrie.insert(d, "")
    } in { case (dataset, ctrie) =>
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.insert(d, "")
        i += 1
      }
    }

    using(ctries) curve("snapshot-support") tearDown { case (dataset, ctrie) =>
      //for (d <- dataset) ctrie.update(d, "")
    } in { case (dataset, ctrie) =>
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.update(d, "")
        i += 1
      }
    }

    using(ctries) curve("after-snapshot") tearDown { case (dataset, ctrie) =>
      //for (d <- dataset) ctrie.update(d, "")
    } in { case (dataset, ctrie) =>
      ctrie.snapshot()
      var i = 0
      val size = dataset.length
      while (i < size) {
        val d = dataset(i)
        ctrie.update(d, "")
        i += 1
      }
    }
  }

}


object CtrieMemoryFootprint
extends PerformanceTest.OfflineReport {
  val sizes = Gen.range("size")(50000, 650000, 100000)
  val datasets = for (size <- sizes) yield (0 until size).map(_.toString).toArray

  override def measurer = new Measurer.MemoryFootprint

  performance of "memory" config(
    exec.minWarmupRuns -> 5,
    exec.maxWarmupRuns -> 10,
    exec.benchRuns -> 10,
    exec.independentSamples -> 1,
    exec.reinstantiation.frequency -> 2
  ) in {
    using(datasets) curve("no-snapshot-support") in { dataset =>
      val ctrie = new simple.ConcurrentTrie[String, String]
      for (d <- dataset) ctrie.insert(d, "")
      ctrie
    }

    using(datasets) curve("snapshot-support") in { dataset =>
      val ctrie = new ConcurrentTrie[String, String]
      for (d <- dataset) ctrie.update(d, "")
      ctrie
    }

    using(datasets) curve("ConcurrentHashMap") in { dataset =>
      val chashmap = new java.util.concurrent.ConcurrentHashMap[String, String]
      for (d <- dataset) chashmap.put(d, "")
      chashmap
    }

    using(datasets) curve("ConcurrentSkipListMap") in { dataset =>
      val cskiplist = new java.util.concurrent.ConcurrentSkipListMap[String, String]
      for (d <- dataset) cskiplist.put(d, "")
      cskiplist
    }
  }

}


object CtrieVsSTM
extends PerformanceTest.OfflineReport {
  class CustomTMap[K, V] {
    class Entry(val k: K, val v: Ref[V], val hc: Int, var next: Entry)
    val table = Ref(TArray.ofDim[Entry](16))
    val size = Ref(0)

    private def resize()(implicit txn: InTxn) {
      val ntable = TArray.ofDim[Entry](table().length * 2)
      for (i <- 0 until table().length) {
        val t = table()
        var curr = t(i)
        while (curr != null) {
          val pos = curr.hc % ntable.length
          ntable(pos) = new Entry(curr.k, curr.v, curr.hc, ntable(pos))
          curr = curr.next
        }
      }
      table() = ntable
    }

    def insert(k: K, v: V)(implicit txn: InTxn) {
      val hc = k.##
      val pos = hc % table().length
      val t = table()

      var entry = t(pos)
      while (entry != null) {
        if (entry.k == k) {
          entry.v() = v
          return
        }
        entry = entry.next
      }

      t(pos) = new Entry(k, Ref(v), hc, t(pos))
      size() = size() + 1
      if (size() > table().length * 0.75) resize()
    }

    def lookup(k: K)(implicit txn: InTxn): V = {
      val hc = k.##
      val pos = hc % table().length
      val t = table()
      var entry = t(pos)
      while (entry != null) {
        if (entry.k == k) return entry.v()
        entry = entry.next
      }
      return null.asInstanceOf[V]
    }
  }

  val sizes = Gen.range("size")(50000, 650000, 100000)
  val datasets = for (size <- sizes) yield (0 until size).map(_.toString).toArray
  val ctries = for (dataset <- datasets) yield {
    val ctrie = new ConcurrentTrie[String, String]
    for (d <- dataset) ctrie.update(d, "")
    (dataset, ctrie)
  }
  val tmaps = for (dataset <- datasets) yield {
    val tmap = TMap[String, String]()
    atomic { implicit txn =>
      for (d <- dataset) tmap.update(d, "")
      (dataset, tmap)
    }
  }
  val tmapsCustom = for (dataset <- datasets) yield {
    val tmap = new CustomTMap[String, String]()
    atomic { implicit txn =>
      for (d <- dataset) tmap.insert(d, "")
      (dataset, tmap)
    }
  }

  def thread(b: =>Unit) = {
    val t = new Thread {
      override def run() = b
    }
    t.start()
    t
  }

  measure method "insert" config(
    exec.minWarmupRuns -> 10,
    exec.maxWarmupRuns -> 20,
    exec.benchRuns -> 16,
    exec.independentSamples -> 1,
    exec.reinstantiation.frequency -> 4
  ) in {
    using(ctries) curve("Ctrie") in { case (dataset, ctrie) =>
      val snap = ctrie.snapshot()
      val threads = for (i <- 0 until 4) yield thread {
        var i = 0
        val size = dataset.length
        while (i < size) {
          val d = dataset(i)
          ctrie.apply(d)
          i += 1
        }
      }
      threads.foreach(_.join())
    }

    using(tmaps) curve("TMap-atomic") in { case (dataset, tmap) =>
      val threads = for (i <- 0 until 4) yield thread {
        atomic { implicit txn =>
          var i = 0
          val size = dataset.length
          while (i < size) {
            val d = dataset(i)
            tmap.apply(d)
            i += 1
          }
        }
      }
      threads.foreach(_.join())
    }

    using(tmaps) curve("TMap-snapshot") in { case (dataset, tmap) =>
      val threads = for (i <- 0 until 4) yield thread {
        val map = atomic { implicit txn =>
          tmap.snapshot
        }
        var i = 0
        val size = dataset.length
        while (i < size) {
          val d = dataset(i)
          map.apply(d)
          i += 1
        }
      }
      threads.foreach(_.join())
    }

    using(tmapsCustom) curve("TMap-custom") in { case (dataset, tmap) =>
      val threads = for (i <- 0 until 4) yield thread {
        atomic { implicit txn =>
          var i = 0
          val size = dataset.length
          while (i < size) {
            val d = dataset(i)
            tmap.lookup(d)
            i += 1
          }
        }
      }
      threads.foreach(_.join())
    }

    using(tmapsCustom) curve("TMap-custom+write") in { case (dataset, tmap) =>
      val threads = for (i <- 0 until 4) yield thread {
        atomic { implicit txn =>
          tmap.insert(dataset(0), "!")
          var i = 0
          val size = dataset.length
          while (i < size) {
            val d = dataset(i)
            tmap.lookup(d)
            i += 1
          }
        }
      }
      threads.foreach(_.join())
    }

  }

}